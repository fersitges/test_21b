import connect from 'react-redux/es/connect/connect';
import { getGames } from '../actions/gamesActions';
import Dashboard from '../components/Dashboard';

const mapStateToProps = state => ({
  games: state.games,
  successful: false,
});

const mapDispatchToProps = dispatch => ({
  onGetGames: () => {
    dispatch(getGames());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Dashboard);
