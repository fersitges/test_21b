import connect from 'react-redux/es/connect/connect';
import _ from 'lodash';
import { getGameRun, getUserDetails, getGameInfo } from '../actions/gamesActions';
import Details from '../components/Details';


const mapStateToProps = state => ({
  currentGame: state.currentGame,
  currentUser: state.currentUser,
  currentGameInfo: state.currentGameInfo,
  games: state.games,
  successful: false,
  emptyRun: state.emptyRun,
});

const mapDispatchToProps = dispatch => ({
  onGetGameRun: (gameId) => {
    dispatch(getGameRun(gameId))
      .then((data) => {
        const runsData = data.value.data.data;
        if (!_.isEmpty(runsData)) {
          const user = runsData[0].players[0].id;
          dispatch(getUserDetails(user));
        }
      });
  },
  onGetGameInfo: (gameId) => {
    dispatch(getGameInfo(gameId));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Details);
