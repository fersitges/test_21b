import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Dashboard extends Component {
  componentDidMount() {
    const { onGetGames } = this.props;
    onGetGames();
  }

  /**
   * Get game run details.
   * @param gameId
   */
  onGetDetails(gameId) {
    const { history } = this.props;
    const location = {
      pathname: `/details/${gameId}`,
    };

    history.push(location);
  }

  render() {
    const { games } = this.props;

    if (games.length === 0) {
      return null;
    }

    const gameList = [];

    games.forEach((gameItem) => {
      gameList.push(
        <li
          key={gameItem.id}
          className="game-card"
        >
          <button type="button" onClick={() => this.onGetDetails(gameItem.id)}>
            <h3>{gameItem.names.international}</h3>
            <div className="pic">
              <img alt={gameItem.names.international} src={gameItem.assets.logo.uri} />
            </div>
          </button>
        </li>,
      );
    });

    return (
      <ul className="game-list">
        {gameList}
      </ul>
    );
  }
}

Dashboard.propTypes = {
  games: PropTypes.array.isRequired,
  onGetGames: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export default Dashboard;
