import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import moment from 'moment';

class Details extends Component {
  componentDidMount() {
    const {
      match, onGetGameRun, onGetGameInfo,
    } = this.props;
    const gameId = match.params.id;

    onGetGameRun(gameId);
    onGetGameInfo(gameId);
  }

  render() {
    const {
      emptyRun, currentGame, currentUser, currentGameInfo,
    } = this.props;

    const location = {
      pathname: '/',
    };
    let runDetails;

    // Current run and user should no be empty if an empty run was detected since the reducer.
    if (emptyRun === false && !_.isEmpty(currentGame) && !_.isEmpty(currentUser)) {
      const runTime = currentGame.times.realtime_t;
      const time = moment().startOf('day').seconds(runTime).format('H:mm:ss');
      runDetails = (
        <Fragment>
          <div>
            {
              currentGame.videos.links.length > 0
                ? (<a href={currentGame.videos.links[0].uri}>Run video</a>) : null
            }
          </div>
          <div>
            Name:
            {' '}
            <b>{currentUser.names.international}</b>
          </div>
          <div>
            Time:
            {time}
          </div>
        </Fragment>
      );
    } else if (emptyRun === true && !_.isEmpty(currentGameInfo)) {
      // However if run is empty, we should still have info from the game.
      runDetails = (
        <div>We&apos;re sorry, this game has no runs!</div>
      );
    } else {
      return null;
    }

    return (
      <div className="game-details">
        <Link to={location}>Go back to game dashboard</Link>
        <h1>{currentGameInfo.names.international}</h1>
        <div>
          <img alt={currentGameInfo.names.international} src={currentGameInfo.assets.logo.uri} />
        </div>
        { runDetails }
      </div>
    );
  }
}

Details.propTypes = {
  onGetGameRun: PropTypes.func.isRequired,
  currentGame: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  onGetGameInfo: PropTypes.func.isRequired,
  games: PropTypes.array.isRequired,
  currentGameInfo: PropTypes.object,
  emptyRun: PropTypes.bool.isRequired,
};

export default Details;
