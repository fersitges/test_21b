// Actions for the redux containers
import axios from 'axios';

export const GET_GAMES = 'GET_GAMES';
export const GET_GAME_RUN = 'GET_GAME_RUN';
export const GET_USER_DETAILS = 'GET_USER_DETAILS';
export const GET_GAME_INFO = 'GET_GAME_INFO';

const apiGameUrl = 'https://www.speedrun.com/api/v1/games';
const apiRunsUrl = 'https://www.speedrun.com/api/v1/runs';
const apiUserUrl = 'https://www.speedrun.com/api/v1/users';

/**
 * Action to get all games from the API
 * @returns {{type: string, payload: *}}
 */
export const getGames = () => ({
  type: GET_GAMES,
  payload: axios.get(apiGameUrl),
});

/**
 * Action to get a single game info from the API
 * @param {int} gameId
 * @returns {{type: string, payload: *}}
 */
export const getGameInfo = gameId => ({
  type: GET_GAME_INFO,
  payload: axios.get(`${apiGameUrl}/${gameId}`),
});

/**
 * Action to get first game run from the API
 * @returns {{type: string, payload: *}}
 */
export const getGameRun = gameId => ({
  type: GET_GAME_RUN,
  payload: axios.get(apiRunsUrl, {
    params: {
      game: gameId,
    },
  }),
});

/**
 * Action to get user details.
 * @param userId
 * @returns {{type: string, payload: AxiosPromise<any>}}
 */
export const getUserDetails = userId => ({
  type: GET_USER_DETAILS,
  payload: axios.get(`${apiUserUrl}/${userId}`),
});
