import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import store from './store';
import DashboardContainer from './containers/DashboardContainer';
import DetailsContainer from './containers/DetailsContainer';
import './App.css';

const App = () => (
  <Provider store={store}>
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={DashboardContainer} />
          <Route path="/details/:id" component={DetailsContainer} />
        </Switch>
      </div>
    </Router>
  </Provider>
);

export default App;
