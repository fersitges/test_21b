import _ from 'lodash';
import {
  GET_GAMES, GET_GAME_RUN, GET_USER_DETAILS, GET_GAME_INFO,
} from '../actions/gamesActions';

const initialState = {
  games: [],
  currentGame: {},
  currentUser: {},
  currentGameInfo: {},
  successful: false,
  emptyRun: false,
};

const games = (state = initialState, action) => {
  switch (action.type) {
    case `${GET_GAMES}_PENDING`: {
      return state;
    }
    case `${GET_GAMES}_FULFILLED`: {
      return Object.assign({}, state, {
        games: action.payload.data.data,
      });
    }
    case `${GET_GAMES}_REJECTED`: {
      return Object.assign({}, state, {
        successful: false,
      });
    }
    case `${GET_GAME_INFO}_PENDING`: {
      return state;
    }
    case `${GET_GAME_INFO}_FULFILLED`: {
      return Object.assign({}, state, {
        currentGameInfo: action.payload.data.data,
      });
    }
    case `${GET_GAME_INFO}_REJECTED`: {
      return Object.assign({}, state, {
        successful: false,
      });
    }
    case `${GET_GAME_RUN}_PENDING`: {
      return state;
    }
    case `${GET_GAME_RUN}_FULFILLED`: {
      const runsData = action.payload.data.data;
      let currentGame;
      let emptyRun = false;

      if (_.isEmpty(runsData)) {
        currentGame = {};
        emptyRun = true;
      } else {
        [currentGame] = runsData;
      }

      return Object.assign({}, state, {
        currentGame,
        emptyRun,
      });
    }
    case `${GET_GAME_RUN}_REJECTED`: {
      return Object.assign({}, state, {
        successful: false,
      });
    }
    case `${GET_USER_DETAILS}_PENDING`: {
      return state;
    }
    case `${GET_USER_DETAILS}_FULFILLED`: {
      return Object.assign({}, state, {
        currentUser: action.payload.data.data,
      });
    }
    case `${GET_USER_DETAILS}_REJECTED`: {
      return Object.assign({}, state, {
        successful: false,
      });
    }
    default:
      return state;
  }
};

export default games;
